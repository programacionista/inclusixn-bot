import unittest

from utils import o_to_x


class TestUtils(unittest.TestCase):

    def test_o_to_x(self):
        self.assertEqual(o_to_x("como"), "cxmx")
        self.assertEqual(o_to_x("COMO"), "CXMX")
        self.assertEqual(o_to_x("cómo estaís amigos"), "cxmx estaís amigxs")
        self.assertEqual(o_to_x("CÓMO ESTAÍS AMIGOS"), "CXMX ESTAÍS AMIGXS")
        self.assertEqual(o_to_x("/incl ahora sí"), "ahxra sí")

if __name__ == '__main__':
    unittest.main()
